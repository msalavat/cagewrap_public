#The collection of bash scripts used in order to demux, trim and map the CAGE raw fastq files. The scrupt for conversion from BAM to bedGraph and eventually to BigWig for importing in to CAGEfightR package is also included in the same file.

#!/bin/bash
#=============================================================================
#FastXToolkit demux example
#Lane 4
#barcode4.list
#UL28 TAG
#UL29 CTT
#UL30 GCC
#UL31 TGG
#UL32 ATG

zcat 4-ULCAGE04_190620_L004_R1.fastq.gz | fastx_barcode_splitter.pl --bcfile barcode4.list --bol --mismatch 0 --prefix ./EC_ --suffix ".fastq" && pigz -p 8 ./EC_*.fastq

#=============================================================================
#Tagduster
#!/bin/bash
for f in ./*.fastq.gz;do
    S1=$( basename -s .fastq.gz ${f} |
        sed 's/EC_//' )
    S2=$( grep -e ${S1} ./barcods.list |
        cut -d' ' -f2 )
    echo ${S2}
    tagdust $f -t 8 -1 B:${S2} -2 F:CAGNNN -3 R:N -4 P:TCGTATGCCGTCTTCTGCTT -dust 100  -o ${f/.fastq.gz/_trimmed.fq.gz}
done;
 
#=============================================================================
#Mapping with bowtie2
# The BowTie2 Indexes were produed for both Oar_Rambv1.0 and OAR3.1 assemblies and for preventing code repetion the Oar_Rambv1.0 included here as example. Other steps were exactly the same. 
#!/bin/bash
for f in ./*.fq.gz;do
        BAMNAME=$(basename -s .fq.gz $f)
        bowtie2 -p 8 --met 1 --very-sensitive -x Oar_ram1 -U $f | samtools view -@ 8 -bS -F 4 | samtools sort -@8 -o ${BAMNAME}.bam
        samtools index ${BAMNAME}.bam
done;

#=============================================================================
#Bedtools bedGraph production 
#!/bin/bash
for f in ./*.bam;do
    NAME=$(basename -s .bam $f)
    bedtools genomecov -ibam $f -d -strand + | awk -v width=1 '!($1~/^NW/)&&($3!=0) {print $1,$2,$2+width,$3}' > ${NAME}.plus.bedGraph &
	bedtools genomecov -ibam $f -d -strand - | awk -v width=1 '!($1~/^NW/)&&($3!=0) {print $1,$2,$2+width,$3}' > ${NAME}.minus.bedGraph
done;

#=============================================================================
#BedGraphToBigWig (UCSC tools)
#reference chromosome length (ref_conv file)
#NC_040261.1 97206449
#NC_040262.1 60977284
#NC_040263.1 84520006
#NC_040264.1 87256642
#NC_040265.1 71110529
#NC_040266.1 90320818
#NC_040267.1 78351214
#NC_040268.1 82584648
#NC_040269.1 70857295
#NC_040270.1 62707586
#NC_040252.1 301310029
#NC_040271.1 55937559
#NC_040272.1 52946818
#NC_040273.1 55669324
#NC_040274.1 68305173
#NC_040275.1 43079388
#NC_040276.1 47656619
#NC_040277.1 49387815
#NC_040253.1 265688841
#NC_040254.1 241140968
#NC_040255.1 130071917
#NC_040256.1 117625569
#NC_040257.1 129788821
#NC_040258.1 107697090
#NC_040259.1 98768168
#NC_040260.1 104713371
#NC_001941.1 16617
#NC_040278.1 153341987

#!/bin/bash
for f in ./*.plus.bedGraph;do
	NAME=$(basename -s .plus.bedGraph $f)
	sort -k 1,1 -k2,2n ${f} > tmp_plus &&
	bedGraphToBigWig tmp_plus ref_conv ${NAME}.plus.bw
	rm -f tmp_plus
done;

for f in ./*.minus.bedGraph;do
	NAME=$(basename -s .minus.bedGraph $f)
	sort -k 1,1 -k2,2n ${f} > tmp_minus &&
	bedGraphToBigWig tmp_minus ref_conv ${NAME}.minus.bw
	rm -f tmp_minus
done;

