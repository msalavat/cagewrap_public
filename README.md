This pipeline is designed for the analysis of the CAGE datasets

1. Mapping (mapping.sh)
2. Demultiplexing the pooled libraries using FASTXToolkit
3. Trimming the reads using TagDust2 HMM model
4. Mapping the reads using bowtie2
5. BAM > BedGragh > BigWig conversion for importing to CAGEfightR
6. CAGEfightR walkthrough modified from https://bioconductor.org/packages/release/bioc/vignettes/CAGEfightR/inst/doc/Introduction_to_CAGEfightR.html

- The CAGE sequencing protocol: https://data.faang.org/api/fire_api/assays/ROSLIN_SOP_CAGE-library-preparation_20190903.pdf 
- The CAGE analysis pipeline: https://data.faang.org/api/fire_api/analysis/ROSLIN_SOP_CAGE_analysis_pipeline_20191029.pdf

DOI: https://doi.org/10.1101/2020.07.06.189480

Publication title: 

Global analysis of transcription start sites in the new ovine reference genome (Oar rambouillet v1.0)

Mazdak Salavati1\*, Alex Caulton2,3\*, Richard Clark4, Iveta Gazova1,5, Tim P. Smith6, Kim C. Worley7, Noelle E. Cockett8, Alan L. Archibald1, Shannon Clarke2, Brenda Murdoch9, Emily L. Clark1 and The Ovine FAANG Consortium

1The Roslin Institute and Royal (Dick) School of Veterinary Studies, University of Edinburgh, Edinburgh, UK
2AgResearch, Invermay Agricultural Centre, Mosgiel, New Zealand
3University of Otago, Dunedin, New Zealand
4Genetics Core, Clinical Research Facility, University of Edinburgh, Edinburgh, UK
5Institute for Genetics and Molecular Medicine, University of Edinburgh, Edinburgh, UK
6USDA, ARS, USMARC, Clay Center, Nebraska, USA
7Baylor College of Medicine, Houston, Texas, USA
8Utah State University, Logan, Utah, USA
9University of Idaho, Moscow, Idaho, USA

*These two authors contributed equally to the work
Corresponding author: emily.clark@roslin.ed.ac.uk
